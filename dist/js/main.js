// Select DOM Items
const menuBtn = document.querySelector('.menu-btn');
const menu = document.querySelector('.menu');
const menuNav = document.querySelector('.menu-nav');
const menuBranding = document.querySelector('.menu-branding');
const navItems = document.querySelectorAll('.nav-item');

// Set Initial State Of Menu
let showMenu = false;

menuBtn.addEventListener('click', toggleMenu);

function toggleMenu() {
  if (!showMenu) {
    menuBtn.classList.add('close');
    menu.classList.add('show');
    menuNav.classList.add('show');
    menuBranding.classList.add('show');
    navItems.forEach(item => item.classList.add('show'));

    // Set Menu State
    showMenu = true;
  } else {
    menuBtn.classList.remove('close');
    menu.classList.remove('show');
    menuNav.classList.remove('show');
    menuBranding.classList.remove('show');
    navItems.forEach(item => item.classList.remove('show'));

    // Set Menu State
    showMenu = false;
  }
}

//Projects button
function myFunction() {
  document.getElementById("demo").innerHTML = "<h4>Ethos Nutri</h4> <p>Bootstrap, Wordpress, PHP, Responsive Website</p><p>Website developed to distributing healtcare supplements</p>";
  document.getElementById("demo1").innerHTML = "<h4>Angular Project</h4> <p></p><p></p>";
  document.getElementById("demo2").innerHTML = "<h4>Angular</h4> <p><strong>Angular 6</strong> woriking with Routing, Services, Components,HTTP Client, Animations</p>";
  document.getElementById("demo3").innerHTML = "<h4>Techscroll</h4><p>JQuery, Javascript, Bootstrap, Responsive Website</p><p>Implemented animations and scrolling effect with JQuery and Javascript</p>";
  document.getElementById("demo4").innerHTML = "<h4>Local Weather App</h4><p>Weather API, Javascript, JQuery, HTML, CSS</p><p></p>";

}